from string import ascii_lowercase, ascii_uppercase, ascii_letters
import os

### TODO: Define the valid alphabets
valid_AA_letters = "ARNDBCQEZGHILKMFPSTWYV"
valid_NA_letters = "ACGTUN"


accepted_characters = {"AA": valid_AA_letters,
                       "NA": valid_NA_letters}

def split_sequences(filename, alphabet="AA"):
    """ Function that reads sequences from a file and writes each sequence back to a separate file.

    :param filename: The name (path) to the file containing the sequences.
    :type  filename: str

    :param alphabet: Indicates whether the sequences are amino acid/protein sequences ("AA") or DNA/RNA sequences ("NA).\
     Default: "AA".
    :type alphabet: "AA"

    :returns: True if the operation was successful, False if not.
    :rtype: bool
    """

    # Test file exists.
    ### COMPLETE ME. Uncomment the next two lines and replace '###' appropriately.
    # if not os.path.###(filename):
    #    raise IOError("{} does not exist.".format(filename))

    # Initialize empty directory to store the sequences into. After successful
    # parsing, return this directory.
    seq_dict = {}

    # Read in the sequences
    with open(filename, 'r') as ihandle:
        for line in ihandle.readlines():
            if line[0] == ">":
                # Line is the header.
                id=line[1:-1]
                ofname = id+".fasta"
                seq_dict[id] = os.path.abspath(ofname)
                continue

            # Line is the sequence.
            ### FIXME: Fix the next line such that existing files are overwritten.:
            with open(ofname, "a") as ohandle:
                for letter in line[:-1]:
                    if letter not in accepted_characters[alphabet]:
                        pass
                        ### FIXME:
                        ### Raise a ValueError with a helpful error message.
                ohandle.write(">"+id+"\n")
                ohandle.write(line)

    ### FIXME: Add a return statement to make the test pass.


